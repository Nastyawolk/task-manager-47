package ru.t1.volkova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUserOwnedDTOModel extends AbstractModelDTO {

    @NotNull
    @Column(name = "user_id", nullable = false)
    private String userId;

}
