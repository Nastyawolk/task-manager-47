package ru.t1.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.model.IUserRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IPropertyService;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.model.IUserService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.EmailEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.LoginEmptyException;
import ru.t1.volkova.tm.exception.field.PasswordEmptyException;
import ru.t1.volkova.tm.exception.user.EmailExistsException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.repository.model.UserRepository;
import ru.t1.volkova.tm.util.HashUtil;

import javax.persistence.EntityManager;

public final class UserService extends AbstractService<User, IUserRepository>
        implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectDTOService projectService;

    @NotNull
    private final ITaskDTOService taskService;

    public UserService(
            @NotNull final IUserRepository repository,
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectDTOService projectService,
            @NotNull final ITaskDTOService taskService) {
        super(repository, connectionService);
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public @NotNull IUserRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setEmail(email);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setRole(role);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User removeOne(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneById(user.getId());
            entityManager.getTransaction().commit();
            @NotNull final String userId = user.getId();
            taskService.clear(userId);
            projectService.clear(userId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            @NotNull final User user = repository.findOneById(id);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            @NotNull final User user = repository.findOneById(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findOneByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findOneByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return this.removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return this.removeOne(user);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findOneByLogin(login) != null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findOneByEmail(email) != null;
        } finally {
            entityManager.close();
        }
    }

}
