package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.repository.dto.UserDTORepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNull;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 4;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final IUserDTORepository userRepository = new UserDTORepository(entityManager);

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull UserDTO user = new UserDTO();
            user.setLogin("user" + i);
            user.setEmail("user@" + i + ".ru");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAddUser() {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final UserDTO newUser = new UserDTO();
        userRepository.add(newUser);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @Nullable final List<UserDTO> userList = userRepository.findAll();
        Assert.assertNotEquals(0, userList.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final UserDTO expected1 = userList.get(1);
        @NotNull final UserDTO expected2 = userList.get(2);
        Assert.assertEquals(expected1, userRepository.findOneById(expected1.getId()));
        Assert.assertEquals(expected2, userRepository.findOneById(expected2.getId()));
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertNull(userRepository.findOneById("NotExcitingId"));
    }

    @Test
    public void testRemoveOne() {
        @Nullable final UserDTO user = userRepository.findOneById(userList.get(0).getId());
        userRepository.removeOneById(user.getId());
        assertNull(userRepository.findOneById(userList.get(0).getId()));
    }

    @Test
    public void testRemoveAll() {
        userRepository.clear();
        Assert.assertEquals(0, userRepository.getSize());
    }


    @Test
    public void testGetSize() {
        Assert.assertNotEquals(0, userRepository.getSize());
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(userList.get(1), userRepository.findOneByLogin("user2"));
        Assert.assertEquals(userList.get(0), userRepository.findOneByLogin("user1"));
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertNull(userRepository.findOneByLogin("user-test"));
    }

    @Test
    public void testFindByEmail() {
        Assert.assertEquals(userList.get(1), userRepository.findOneByEmail("user@2.ru"));
        Assert.assertEquals(userList.get(0), userRepository.findOneByEmail("user@1.ru"));
    }

    @Test
    public void testFindByEmailNegative() {
        Assert.assertNull(userRepository.findOneByEmail("test@ru"));
    }


}
